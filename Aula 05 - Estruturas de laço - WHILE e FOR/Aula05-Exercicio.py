'''
Exercício da Aula 05
Faça um programa que leia a quantidade de pessoas que serão convidadas para uma festa

Leia esse nome de nomes de todas as pessoas (usando while ou for) e colocar numa lista de convidados
Imprimir todos os nomes da lista

Diego Mendes Rodrigues
'''

# Lendo a quantidade de convidados
quantidade = int(input('Quantas pessoas serão convidadas: '))

convidados = []

# Lendo o nome dos convidados
# Começei o i=1, ao invés de 0, por causa da exibição
# tive que terminar com i <= quantidade, ao invés de i < quantidade
print('\n')
i = 1
while i <= quantidade:
    convidados.append(input('Nome do convidado ' + str(i) + ': '))
    i += 1

# Exibindo os convidados
print('\nConvidados:')
convidados.sort()
for nome in convidados:
    print(nome)