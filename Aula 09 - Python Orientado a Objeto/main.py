'''
Programa principal OO
Programa de Veiculos (Carro, moto, caminhão...)

Diego Mendes Rodrigues
'''

# Um objeto sempre terá características e funções/métodos

# De dentro do arquivo veiculo importe a classe Veiculo
from carro import Carro
from veiculo import Veiculo

# Criando o primeiro objeto com a classe Veiculo
caminhao_azul = Veiculo('azul', 8, 'Ford', 10)

print(caminhao_azul)        # <veiculo.Veiculo object at 0x0076E2B0>
print(type(caminhao_azul))  # Criamos um novo tipo no Pythom <class 'veiculo.Veiculo'>

caminhao_azul.cor = 'Azul'
print(caminhao_azul.cor)
print('\n')

print('CAMINHÃO')
print('Cor....:', caminhao_azul.cor)
print('Rodas..:', caminhao_azul.rodas)
print('Marca..:', caminhao_azul.marca)
print('Tanque.:', caminhao_azul.tanque)
print('\n')

# Criando o segundo objeto, com a classe Carro
# Observar que não colocamos o numero de rodas, pois na classe Carro já definimos como 4
carro_branco = Carro('Branco', 'BMW', 30)

print('CARRO')
print('Cor....:', carro_branco.cor)
print('Rodas..:', carro_branco.rodas)
print('Marca..:', carro_branco.marca)
print('Tanque.:', carro_branco.tanque)
print('\n')

# Abastecendo o carro_branco
carro_branco.abastercer(18)
print('CARRO (Abastecido)')
print('Tanque.:', carro_branco.tanque)
print('\n')

# Abastecendo o carro_branco
carro_branco.abastercer(20)
print('CARRO (Abastecido)')
print('Tanque.:', carro_branco.tanque)
print('\n')

