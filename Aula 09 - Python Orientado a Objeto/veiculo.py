'''
Objeto Veiculo

Iremos criar uma Classe
O objeto é quando pegamos a classe e instanciamos

Diego Mendes Rodrigues
'''

# Nome de Classe começa com Letra Maiuscula
class Veiculo:
    # Método construtor
    def __init__(self, cor, rodas, marca, tanque):
        self.cor    = cor
        self.rodas  = rodas
        self.marca  = marca
        self.tanque = tanque

    def abastercer(self, litros):
        self.tanque += litros