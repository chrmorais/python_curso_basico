'''
Exemplo de uso das variaveis nas classes

Diego Mendes Rodrigues
'''

class Cachorro():
    tipo = 'Canino' # Variável tipo que é a mesma em todas as instâncias

    # Contrutor onde definimos o Nome do Cachorro
    def __init__(self,nome:str=''):
        self.nome = nome    # Variável nome para cada instância da classe

# Instanciando a classe para dois cachorros
cao1 = Cachorro('Bart')
cao2 = Cachorro('Platão')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.tipo )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.tipo )

# Observer que a propriedade .tipo é a mesma nas duas instâncias!
# Isso pode ser um problema nos scripts Python

# Caso eu queira alterar o Tipo do cao2
cao2.tipo = 'Filhote'
print('\nAlterei o .tipo da instância cao2\n')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.tipo )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.tipo )

# Caso eu queira alterar o Tipo de todos os cachorros
cao2 = Cachorro('Platão')   # Desfazendo a alteração anterior

Cachorro.tipo = 'Adulto'   # Alterando tipo da Classe Cachorro

print('\nAlterei o .tipo na Classe Cachorro\n')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.tipo )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.tipo )

# Conclusão:
# O ideal, na maioria das vezes, é definir as variáveis na função construtora __init__(self,...) da Classe