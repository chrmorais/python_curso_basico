'''
Exemplo de uso das variaveis nas classes

Diego Mendes Rodrigues
'''

class Cachorro():
    truques = []    # Lista que é a mesma em todas as instâncias

    # Contrutor onde definimos o Nome do Cachorro
    def __init__(self,nome:str=''):
        self.nome = nome    # Variável nome para cada instância da classe

    def adicionar_truque(self,novo_truque:str=''):
        self.truques.append(novo_truque)

# Instanciando a classe para dois cachorros
cao1 = Cachorro('Bart')
cao2 = Cachorro('Platão')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.truques )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.truques )

# Observer que a propriedade (Lista) .truques é a mesma nas duas instâncias!
# Isso pode ser um problema nos scripts Python

# Veremos esse problema nesse exemplo!

# Vou incluir um truque no cao1
cao1.adicionar_truque('Pular')
print('\nAdicionei o truque Pular da instância cao1\n')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.truques )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.truques )

print('')
print('# Observe que os 2 cães possuem o mesmo truque, embora eu tenha adicionado apelas no cao1!!')

print('')
print('')


# Vou incluir um truque no cao2
cao2.adicionar_truque('Deitar')
print('\nAdicionei o truque Deitar da instância cao2\n')

# Exibindo
print('Cão 1\n----------')
print( cao1.nome )
print( cao1.truques )

print('')

print('Cão 2\n----------')
print( cao2.nome )
print( cao2.truques )

print('')
print('# Observe que os 2 cães possuem os dois truques!!\n'
      '# Eu adicioneu 1 truque no cao1 e 1 truque no cao2\n'
      '# Essa Lista compartilhada provavelmente irá causar problemas nos meus scripts')

# Conclusão:
# O ideal, na maioria das vezes, é definir as variáveis e Listas
# na função construtora __init__(self,...) da Classe
# Listas compartilhadas provavelmente irão gerar problemas nos scripts Python

# Forma correta de definir essa classe
'''
class Cachorro():

    # Contrutor onde definimos o Nome do Cachorro e criamos a Lista de Truques
    def __init__(self,nome:str=''):
        self.nome = nome    # Variável nome para cada instância da classe
        self.truques = []   # Uma Lista limpa para cada Cachorro

    def adicionar_truque(self,novo_truque:str=''):
        self.truques.append(novo_truque)
'''