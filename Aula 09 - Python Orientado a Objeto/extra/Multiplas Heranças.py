'''
Uma Classe pode Herdar (usar herança) de outra Classe
ou
Uma Classe pode Herdar de várias outras Classes
'''

# Criando as Classes Imovel, Proprietario e Residencial
class Imovel():
    def __init__(self, id:int=0, descricao:str=''):
        self.id = id
        self.descricao = descricao

class Proprietario():
    def __init__(self, proprietario_nome:str='', proprietario_cpf:str=''):
        self.proprietario_nome = proprietario_nome
        self.proprietario_cpf = proprietario_cpf

class Residencial():
    def __init__(self,area:float=0, quartos:int=0, banheiros:int=0, salas:int=0, comodos:int=0):
        self.area = area
        self.quartos = quartos
        self.banheiros = banheiros
        self.salas = salas
        self.comodos = comodos


# Criando a Classe Casa com multipla herança
# --------------------------------------------------------------------------------------------
class Casa(Imovel, Proprietario, Residencial):
    def __init__(self, id:int = 0, descricao:str = '',
                 proprietario_nome: str = '', proprietario_cpf: str = '',
                 area: float = 0, quartos: int = 0, banheiros: int = 0, salas: int = 0, comodos: int = 0):
        Imovel.__init__(self, id, descricao)
        Proprietario.__init__(self, proprietario_nome, proprietario_cpf)
        Residencial.__init__(self, area, quartos, banheiros, salas, comodos)


# Instanciando a Classe Casa()
casa_regina = Casa()
print('CASA_REGINA sem definir os atributos\n-------------------------')
print('Id.........:', casa_regina.id)
print('Descrição..:', casa_regina.descricao)
print('Prop. Nome.:', casa_regina.proprietario_nome)
print('Prop. CPF..:', casa_regina.proprietario_cpf)
print('Área.......:', casa_regina.area)
print('Quartos....:', casa_regina.quartos)
print('Banheiros..:', casa_regina.banheiros)
print('Salas......:', casa_regina.salas)
print('Comodos....:', casa_regina.comodos)
print('')

# Colocando atributos na Casa
casa_regina.id = 8547
casa_regina.descricao = 'Casa bem localizada no condomínio Raizes'
casa_regina.proprietario_nome = 'Regina Marcia Ruiz'
casa_regina.proprietario_cpf = '047.874.887-98'
casa_regina.area = 150
casa_regina.quartos = 3
casa_regina.banheiros = 3
casa_regina.salas = 2
casa_regina.comodos = 13

# Exibindo
print('CASA_REGINA com atributos\n-------------------------')
print('Id.........:', casa_regina.id)
print('Descrição..:', casa_regina.descricao)
print('Prop. Nome.:', casa_regina.proprietario_nome)
print('Prop. CPF..:', casa_regina.proprietario_cpf)
print('Área.......:', casa_regina.area)
print('Quartos....:', casa_regina.quartos)
print('Banheiros..:', casa_regina.banheiros)
print('Salas......:', casa_regina.salas)
print('Comodos....:', casa_regina.comodos)
print('')

# Definindo os atributos na função construtora
casa_diego = Casa(7414,'Apartamento no Morumbi','Diego Mendes Rodrigues',
                  '258.874.225-88',60,2,1,1,8)

# Exibindo
print('CASA_DIEGO - atributos na função construtora\n-------------------------')
print('Id.........:', casa_diego.id)
print('Descrição..:', casa_diego.descricao)
print('Prop. Nome.:', casa_diego.proprietario_nome)
print('Prop. CPF..:', casa_diego.proprietario_cpf)
print('Área.......:', casa_diego.area)
print('Quartos....:', casa_diego.quartos)
print('Banheiros..:', casa_diego.banheiros)
print('Salas......:', casa_diego.salas)
print('Comodos....:', casa_diego.comodos)