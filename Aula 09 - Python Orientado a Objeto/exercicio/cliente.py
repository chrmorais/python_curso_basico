'''
Classe de Clientes do Banco

Diego Mendes Rodrigues
'''

# Módulo para gerar números aleatórios (ID do Cliente)
from random import *

# Classe para os Clientes do Banco
class Cliente():
    # Construtor
    def __init__(self, nome:str, cpf:str, idade:int):
        self.nome = nome
        self.cpf = cpf
        self.idade = idade
        self.id = randint(100,1000)