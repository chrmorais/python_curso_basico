'''
Classe Conta Bancária

Diego Mendes Rodrigues
'''

# Classe Conta Bancária
class Conta():
    # Construtor
    def __init__(self, cliente_id:int, saldo:int=0, limite:int=0):
        self.cliente_id = cliente_id
        self.saldo = saldo
        self.limite = limite

    # Função de Saque
    def sacar(self,valor:int=0):
        if valor <= 0:
            print('O valor do saque deve ser positivo')
        else:
            pode_sacar = self.saldo + self.limite
            if valor > pode_sacar:
                print('O maior valor que pode ser sacado é R$', pode_sacar)
            else:
                self.saldo -= valor
                print('#-# Saque.: R$', valor)

    # Função de Depósito
    def depositar(self,valor:int=0):
        if valor <= 0:
            print('O valor do depósito deve ser positivo')
        else:
            self.saldo += valor
            print('#+# Depósito.: R$', valor)

    # Método para exibir o Salvo e o Valor Disponivel do Cliente
    # Valor Disponivel = Saldo + Limite
    def ver_saldo(self):
        print("#@# Saldo......: R$", self.saldo)
        print('#@# Disponível.: R$', self.saldo + self.limite)