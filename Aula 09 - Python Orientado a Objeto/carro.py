'''
Herança
'''

from veiculo import Veiculo

# Classe Carro Herda (deriva) todas as características da classe Veiculo
class Carro(Veiculo):
    # Método construtor
    def __init__(self, cor, marca, tanque):         # Todo carro possui 4 rodas
        Veiculo.__init__(self, cor, 4, marca, tanque)     # Todo carro possui 4 rodas

    # Sobrepondo o método abastercer
    def abastercer(self, litros):
        if self.tanque + litros > 50:
            print('## Erro, tanque pode ter apenas 50 litros')
        else:
            self.tanque += litros