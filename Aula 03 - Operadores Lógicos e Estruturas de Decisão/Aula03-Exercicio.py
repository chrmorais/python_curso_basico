'''
Exercício da Aula 03
Perguntar Idade, Peso e Altura de uma pessoa
Decidir se ela está apta para ser do Exército
Para entrar no Exército é preciso ter mais de 18 anos, pesar mais ou igual a 60kg, medir mais ou igual a 1,70m

Diego Mendes Rodrigues
'''

idade   = input('Sua Idade: ')
peso    = input('Seu peso: ')
altura  = input('Sua altura: ')

idade   = int(idade)
peso    = float(peso)
altura  = float(altura)

if ( idade >= 18) and ( peso >= 60) and ( altura >= 1.7):
    print('Você pode entrar no Exército')
else:
    print('Você não pode entrar no Exército')