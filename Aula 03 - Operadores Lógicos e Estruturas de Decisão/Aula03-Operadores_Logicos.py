'''
Operadores Lógicos no Python

Diego Mendes Rodrigues
'''

# Dados Boolean (Boleanos)
verdade = True
falso   = False
print(type(verdade), type(falso))
print(verdade, falso)

# IF
# Identação com 4 espaços
print('\n')
if verdade == True:
    print('Verdade está OK! (', verdade,')')
else:
    print('Verdade NÃO está OK! (', verdade,')')

# OPERADORES (Comparações)
# == != > >= < <=
print('\n')
print('2==2', 2==1)
print('2==2', 2==2)
print('3!=1', 3!=1)
print('3!=3', 3!=3)
print('4>1', 4>1)
print('4>4', 4>4)
print('4>=4', 4>=4)

