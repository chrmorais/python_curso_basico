'''
Usando o not (Não)

Diego Mendes Rodrigues
'''

# Sem usar o not
idade = 50
if idade == 50:
    print('Você possui 50 anos')
else:
    print('Você não possui 50 anos, possui', idade, 'anos')

# USANDO o not
idade = 60
if not idade == 50:
    print('Você não possui 50 anos, possui', idade, 'anos')
else:
    print('Você possui 50 anos')