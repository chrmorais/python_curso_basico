'''
Criando uma Calculadora
Entrada de dados com a função input()
O input() sempre retorna um objeto do tipo string

Diego Mendes Rodrigues
'''

numero1 = input('Número 1: ')
numero2 = input('Número 2: ')

# Soma
resultado = int(numero1) + int(numero2)
print(numero1, '+', numero2, '=', resultado)

# Subtração
resultado = int(numero1) - int(numero2)
print(numero1, '-', numero2, '=', resultado)

# Multiplicação
resultado = int(numero1) * int(numero2)
print(numero1, '*', numero2, '=', resultado)

# Divisão
resultado = int(numero1) / int(numero2)
print(numero1, '/', numero2, '=', resultado)

# Elevando ao quadrado
resultado = 10 ** 2
print('10 ao quadrado=', resultado)

# Raiz quadrada
resultado = 9 ** (1/2)
print('Raiz quadrada de 9 =', resultado)

# Parte Inteira da Divisão
resultado = int(numero1) // int(numero2)
print(numero1, '//', numero2, '=', resultado)

# Resto da Divisão
resultado = int(numero1) % int(numero2)
print(numero1, '%', numero2, '=', resultado)