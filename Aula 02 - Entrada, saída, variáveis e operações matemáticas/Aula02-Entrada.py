'''
Entrada de dados com a função input()
O input() sempre retorna um objeto do tipo string

Diego Mendes Rodrigues
'''

# Solicitando o nome com o input() sem argumentos
print('Escreva seu nome: ')
nome = input()

# Solicitando a idade colocando a mensagem dentro da função input()
idade = input('\nEscreva sua idade: ')

# Exibindo o resultado
print('\n')
print(nome, 'possui', idade, 'anos')

# Iremos verificar se o input() sempre retorna objetos de string
print('\n')
print(type(nome), type(idade))