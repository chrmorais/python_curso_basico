'''
Tratamento de Erros e Exceções no Python
TRY
EXCEPT

Quando não tratamos os erros, quando ele ocorre o programa trava e sai

Diego Mendes Rodrigues
'''

# Exemplo: a = 1200 / 0
# Não podemos dividir por zero, teremos o erro
# ZeroDivisionError: division by zero
# Vamos tratar:
try:    # Tentando executar
    a = 1200 / 0
except: # Ocorreu uma exceção
    print('\nDivisão por zero não pode ser executada')
# Não tivemos o erro, o problema foi tratado!!!
# Dessa forma o programa não trava e continua sendo executado


# Podemos setar qual exceção será tratada
# Vamos tratar apenas a exceção ZeroDivisionError
try:
    a = 1200 / 0
except ZeroDivisionError:
    print('\nDivisão por 0 não pode ser executada')
except:
    print('\nOcorreu um erro na divisão')

try:
    a = 1200 / a
except ZeroDivisionError:
    print('\nDivisão por 0 não pode ser executada')
except:
    print('\nOcorreu um erro na divisão')

# Podemos pegar qual foi o erro
try:
    a = 1200 / a
except Exception as erro:
    print('\nOcorreu um erro na divisão')
    print('Erro:',erro)

try:
    funcaolouca()
except Exception as erro:
    print('\nOcorreu um erro na divisão')
    print('Erro:',erro)


# Quando utilizar try / except
# -------------------------------------------
'''
1. Quando for trabalhar com redes, fazer requisições de redes (sockets...)
2. Abrir e fechar arquivos (open, close...)
3. Contas

try:
    arquivo = open('arquivoquenaoexiste.txt')
except Exception as erro:
    print('Erro ao abrir o arquivo:', erro)

try:
    arquivo.close()
except Exception as erro:
    print('Erro ao fechar o arquivo:', erro)
'''
