'''
Iremos abrir, ler e fechar arquivos
Utilizaremos a verificação de erros

Diego Mendes Rodrigues
'''

# Função que abre o arquivo de maneira segura
def abre_arquivo(arquivo:str):
    try:
        arq = open(arquivo)
        return arq
    except Exception as erro:
        print('Falha ao abrir', arquivo)
        print('Erro:', erro)

# Função que fechar o arquivo de maneira segura
def fecha_arquivo(arquivo):
    try:
        arquivo.close()
        return True
    except Exception as erro:
        print('Falha ao fechar o arquivo')
        print('Erro:', erro)

# Função que lê o arquivo de maneira segura
def ler_arquivo(arquivo):
    try:
        dados = arquivo.read()
        return dados
    except Exception as erro:
        print('Falha ao ler', arquivo)
        print('Erro:', erro)

# Solicitando o nome do arquivo para o usuário final
arquivo_cadastro = input('Nome do arquivo (cadastro.txt): ')
if arquivo_cadastro == '':
    arquivo_cadastro = 'cadastro.txt'

arquivo_cadastro_completo  = 'C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\'
arquivo_cadastro_completo += 'Aula 10 - Entrada e saída de arquivos\\' + arquivo_cadastro

# Abrindo o arquivo
cadastro = abre_arquivo(arquivo_cadastro_completo)
if cadastro:
    print('Arquivo aberto')

# Lendo o arquivo
dados_lidos = ler_arquivo(cadastro)
if dados_lidos:
    print('')
    print('Dados lidos:\n------------')
    print(dados_lidos)
    print('')

# Fechando o arquivo
fechado = fecha_arquivo(cadastro)
if fechado:
    print('Arquivo fechado')