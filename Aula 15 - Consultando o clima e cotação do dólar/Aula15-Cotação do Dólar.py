'''
Fazendo a Cotação do Dólar em tempo real

Utilizaremos a Cotação API
http://api.promasters.net.br/cotacao/

GET http://api.promasters.net.br/cotacao/v1/valores?moedas=USD&alt=json
{
	"status": true,
	"valores": {
		"USD": {
			"nome": "Dólar",
			"valor": 2.333,
			"ultima_consulta": 1386349203,
			"fonte": "UOL Economia - http://economia.uol.com.br/cotacoes/"
		}
	}
}


GET http://api.promasters.net.br/cotacao/v1/valores
{
	"status": true,
	"valores": {
		"USD": {
			"nome": "Dólar",
			"valor": 2.333,
			"ultima_consulta": 1386349203,
			"fonte": "UOL Economia - http://economia.uol.com.br/cotacoes/"
		},
		"EUR": {
			"nome": "Euro",
			"valor": 3.195,
			"ultima_consulta": 1386349203,
			"fonte": "UOL Economia - http://economia.uol.com.br/cotacoes/"
		},
		"BTC": {
			"nome": "Bitcoin",
			"valor": 2620,
			"ultima_consulta": 1386352803,
			"fonte": "Mercado Bitcoin - http://www.mercadobitcoin.com.br/"
		}
	}
}



Diego Mendes Rodrigues
'''

import requests
import json
from datetime import *
import time

while True:
    try:
        # Realizando a requisição
        req = requests.get('http://api.promasters.net.br/cotacao/v1/valores?moedas=USD&alt=json')
        conteudo_json = req.text

        # Convertendo o resultado em um Dicionário JSON
        dicionario = json.loads(conteudo_json)

        # Pegando o valor da Cotação do Dólar
        valores = dicionario['valores']
        cotacao = valores['USD']['valor']
        fonte   = valores['USD']['fonte']

        # Imprimindo
        print('')
        print('Dólar Comercial - Taxa de Venda')
        print('-------------------------------')
        print('US$ 1,00 = R$', str(cotacao).replace('.',','))
        print('Fonte:', fonte)

    except Exception as erro:
        print('Erro ao conectar ao site api.promasters.net.br:')
        print(erro)

    data_hora_agora = datetime.now()
    print('%d/%d/%d - %d:%d:%d' %(data_hora_agora.day, data_hora_agora.month, data_hora_agora.year,
                                      data_hora_agora.hour, data_hora_agora.minute, data_hora_agora.second))

    print('\nNova consulta em 10s\n<Ctrl>+C ou <Ctrl>+<F12> para sair\n')
    time.sleep(10)