'''
List Comprehension é um recurso do Python que permite a criação de listas de forma simples, usando simplesmente
uma expressão seguida de um for que pode conter outros for e/ou if dentro dele.

Criando uma Matriz

Fonte: http://www.programeempython.com.br/blog/estruturas-de-dados-em-python-listas-list-comprehention/

Diego Mendes Rodrigues
'''

print('Lista criada')
l = [1, 2, 4]
print(l)
print('\n')


print('[3 * x for x in l]')
l = [3 * x for x in l]
print(l)
print('\n')


print('[2 * x for x in l if x > 3]')
l = [2 * x for x in l if x > 3]
print(l)
print('\n')


print('[2 * x for x in l if x > 3]')
l = [2 * x for x in l if x > 3]
print(l)
print('\n')


print('[[x, 2*x] for x in l]')
l.clear()
l = [1, 2, 4]
l = [[x, 2*x] for x in l]
print(l)
print('\n')


print('[ x * y for x in l1 for y in l2 ]')
l.clear()
l1 = [0, 1, 2]
l2 = [1, 2, 3]
l = [ x * y for x in l1 for y in l2 ]
print(l)
print('\n')


print('[ l1[i] * l2[i] for i in range(len(l1)) ]')
l.clear()
l = [ l1[i] * l2[i] for i in range(len(l1)) ]
print(l)


# ------------------------------------------------------------------
# MATRIZ no Python
# ------------------------------------------------------------------
print('\n')
print('\nUtilizando uma MATRIZ')
print('---------------------------')

m = [[1, 2, 3],[2, 3, 4]]
print(m)
print('\n')

m = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]
print(m)
print('\n')

print(m[1])
print('\n')

print(m[1][1])
print(m[1][2])