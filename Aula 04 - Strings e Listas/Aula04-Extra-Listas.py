'''
Métodos para utilizar com Listas
Fonte: http://www.programeempython.com.br/blog/estruturas-de-dados-em-python-listas/

Diego Mendes Rodrigues
'''

# Criando uma lista vazia
lista = []


# Colocando informações (valores) na lista
print('CRIANDO A LISTA')
# Acrescentar valores à lista criada anteriormente
# append(o) insere um objeto o no final da lista
lista.append(1)     # a lista ficará assim [1]
lista.append(3)     # a lista ficará assim [1, 3]
print(lista)


print('\nINSERT(i,o)')
# insert(i, o), insere o objeto o na posição i (a primeira posição é zero)
lista.insert(1, 2)  # a lista ficará assim [1, 2, 3]
print(lista)
lista.insert(0, 5)  # a lista ficará assim [5, 1, 2, 3]
print(lista)


print('\nAPPEND(o) e REMOVE(o)')
# remove(o) remove a primeira ocorrência do objeto o
lista.append(5)     # a lista ficará assim [5, 1, 2, 3, 5]
print(lista)
lista.remove(5)     # a lista ficará assim [1, 2, 3, 5]
print(lista)


print('\nPOP(i) e POP()')
# pop(i) remove e retorna o objeto da posição i
# pop() remove e retorna o último elemento da lista
lista.insert(3, 6)  # a lista ficará assim [1, 2, 3, 6, 5]
print(lista)
print(lista.pop(3)) # a lista ficará assim [1, 2, 3, 5] e retornará 6
print(lista)
print(lista.pop())  # a lista ficará assim [1, 2, 3] e retornará 5
print(lista)


print('\nINDEX(o)')
# index(o) retorna o índice do objeto o
print(lista)
print(lista.index(3))   # retorna 2
lista.append(3)         # a lista ficará assim [1, 2, 3, 3]
print(lista)
print(lista.index(3))   # continua retornando 2 pois é a primeira ocorrência do 3


print('\nCOUNT(o)')
# count(o) conta quantas vezes o objeto o aparece na lista
print(lista)
print(lista.count(1))                   # retorna 1
print(lista.count(3))                   # retorna 2
print('pop() -> ' + str(lista.pop()))   # a lista ficará assim [1, 2, 3] e retorna 3
print(lista)
print(lista.count(3))                   # retorna 1


print('\nEXTEND(L)')
# extend(L) estende a lista com a lista L
print(lista)
lista.extend([0, -1, -2]) # a lista ficará assim [1, 2, 3, 0, -1, -2]
print(lista)


print('\nSORT()')
# sort() organiza os itens da lista nela mesma
lista.sort()    # a lista ficará assim [-2, -1, 0, 1, 2, 3]
print(lista)


print('\nREVERSE()')
# reverse() inverte os elementos da lista nela mesma
lista.reverse() # a lista ficará assim [3, 2, 1, 0, -1, -2]
print(lista)