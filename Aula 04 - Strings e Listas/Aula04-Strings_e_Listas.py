'''
Trabalhando com Strings e Listas

Diego Mendes Rodrigues
'''

# STRING
# ------
frase = 'Oi, tudo bem?'
print(frase)

# A frase é uma lista de caracteres

# Imprimindo a primeira e a segunda letra da lista frase
print(frase[0])
print(frase[1])

print('\n')

# LISTA
# -----
# Uma lista é uma coleção, podemos colocar várias coisas dentro de uma lists
lista_nomes = ['Diego', 'Regina', 'Bruna', 'Natalia']

print(lista_nomes[0])
print(lista_nomes[1])
print(lista_nomes[2])
print(lista_nomes)

print('\n')
lista_mista = ['Diego', 'Regina', 'Bruna', 34, 54, 28, 1.7, 1.65, 1.60]
print(lista_mista[1])
print(lista_mista[4])
print(lista_mista[7])

# Último elemento
print('\n')
print(frase[12])
print(frase[-1])

# Imprimindo vários elementos
print('\n')
print(frase[4:8])
print(frase[0:13])
print(frase[0:13:1]) # Colocando o step, o padrão é 1
print(frase[0:13:2]) # Colocando o step 2
print(frase[0:13:10]) # Colocando o step 10
print(frase[0:13:20]) # Colocando o step 20

print('\n')
print(lista_nomes[0:2])
print(lista_nomes[0:3])
print(lista_nomes[0:4])
print(lista_nomes[1:4])
print(lista_nomes[0:4:2]) # Colocando o step 2
print(lista_nomes[-1])
print(lista_nomes[-3])
print(lista_nomes[-1:-5:-1]) # Usando como passo um valor negativo

# Escrevendo ao contrário
print('\n')
print(frase[::-1])
print(lista_nomes[::-1])