'''
Operações com Listas

Diego Mendes Rodrigues
'''

frase = 'Oi, tudo bem?' # A frase é uma lista de caracteres
lista_nomes = ['Diego', 'Regina', 'Bruna', 'Natalia']
print('Lista original\n')
print(lista_nomes)

# Adicionando itens na Lista
lista_nomes.append('Sergio')
lista_nomes.append('Jerome')
print('\nAdicionando\n')
print(lista_nomes)

# Removendo itens da Lista
lista_nomes.remove('Bruna')
print('\nRemovendo\n')
print(lista_nomes)

# Ordenando a Lista
lista_nomes.sort()
print('\nOrdenando\n')
print(lista_nomes)

# Invertendo a Lista
lista_nomes.reverse()
print('\nInvertendo (Revertendo)\n')
print(lista_nomes)

# Inserindo um elemento
# o método .append(<o que irei inserir>) adiciona no final da lista
# o método .insert(<local>,<o que irei inserir>) adiciona em um determinado local
lista_nomes.insert(1,'Paula')
print('\nInserindo Paula na posição 1\n')
print(lista_nomes)

# Alterando um elemento da lista
lista_nomes[0] = 'Irene'
print('\nAlterando o ítem [0]\n')
print(lista_nomes)

# Contando o número de um determinado ítem na Lista
quantas_irenes = lista_nomes.count('Irene')
print('\n')
print(lista_nomes)
print('Irenes:',quantas_irenes)
# -
lista_nomes.append('Irene')
lista_nomes.append('Diego')
lista_nomes.append('Irene')
quantas_irenes = lista_nomes.count('Irene')
print(lista_nomes)
print('Irenes:',quantas_irenes)

# Quanto elementos temos na Lista lista_nomes
print('\n')
print(lista_nomes)
print('Totan de nomes:',len(lista_nomes))

# .pop()
# Pega o último valor da Lista e retira esse valor da Lista
print('\n.pop()\n')
print(lista_nomes)
print(lista_nomes.pop())
print(lista_nomes)
print(lista_nomes.pop())
print(lista_nomes)