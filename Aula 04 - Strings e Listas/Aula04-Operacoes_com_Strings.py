'''
Operações com Strings

Diego Mendes Rodrigues
'''

frase = 'Oi, tudo bem?' # A frase é uma lista de caracteres
print('Frase original\n')
print(frase)

# Upper e Lower
print('\n')
print(frase.lower())
print(frase.upper())

# Transformando a frase em uma Lista
lista = frase.split(' ')
print('\n')
print(lista)
lista2 = frase.split(',')
print(lista2)

# Substituindo
print('\n')
print(frase.replace('bem','ótimo'))

# Concatenando
print('\n')
frase_nova = frase + ' Como vai você?'
print(frase_nova)

# TRIM numa string
frase_com_espaco = ' Diego '
print('\n')
print('|' + frase_com_espaco + '|')
print('|' + frase_com_espaco.strip() + '|')
frase_com_espaco = '    Diego  '
print('|' + frase_com_espaco + '|')
print('|' + frase_com_espaco.strip() + '|')