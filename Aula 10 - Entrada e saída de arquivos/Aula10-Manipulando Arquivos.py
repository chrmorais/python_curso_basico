'''
Manipulando Arquivos no Python

Diego Mendes Rodrigues
'''

# open('arquivo.txt') irá abrir o arquivo.txt que está na mesma pasta que o Python
#
# Podemos definir o caminho do arquivo
# open('c:\\arquivo.txt')
#
# Para criar o arquivo, usar 'w'
# open('c:\\arquivo.txt','w')
#
# O segundo argumento pode ser
# 'r' - leitura (read), caso o arq não exista dá problema
# 'w' - escrita (write) sobrescrevendo o arquivo caso ele já exista, cria o arquivo caso ele não exista
# 'r+' - Leitura e escrita - Não sobreescreve, caso o arq não exista dá problema
# 'a' - escrita no modo adicionar (append) - Não sobreescreve, e cria o arquivo caso ele não exista
#
# 'b' - abre arquivos que não são de texto, são binários, ex: imagem, musica, etc
# O 'b' precisa ser combinado com r, w, a
# 'rb', 'wb', 'ab'
#
# open() retorna um objeto do tipo file (io.TextIOWrapper)


# Abrindo e Criando o arquivo.txt
# -------------------------------------------------
arquivo = open('C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\arquivo.txt','w')

#print(arquivo)
#print(type(arquivo))

texto='Números pares:\n'

# Alterando o Encoding do texto
#texto.encode('iso-8859-1')

arquivo.write(texto)
for i in range(0,11,2):
    arquivo.write(str(i) + '\n')

# Fechando o arquivo
arquivo.close()


# Abrindo e Lendo o arquivo.txt inteiro
# -------------------------------------------------
arquivo = open('C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\arquivo.txt','r')

print( arquivo.read() )

# Fechando o arquivo
arquivo.close()


# Abrindo e Lendo o arquivo.txt Linha por Linha
# -------------------------------------------------
arquivo = open('C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\arquivo.txt','r')

for linha in arquivo:
    print( linha )

# Fechando o arquivo
arquivo.close()


# Abrindo e Lendo um arquivo Binario (imagem jpg)
# -------------------------------------------------
arquivo = open('C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\Captain.jpg','rb')

# Exibindo o codigo hexadecimal do arquivo binario (imagem)
print( arquivo.read() )

# Fechando o arquivo
arquivo.close()