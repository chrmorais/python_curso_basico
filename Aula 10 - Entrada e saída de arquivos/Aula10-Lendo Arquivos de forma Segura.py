'''
Usando with o na abertura do arquivo
Caso esse arquivo tenha sido encerrado incorretamente, o with faz com que a excessão não seja exibida

Fonte: https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files

Diego Mendes Rodrigues
'''

with open('C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\arquivo.txt','r') as arquivo:
    dados = arquivo.read()

print('Fechado?\n------------------')
print(arquivo.closed)

print('\nInformações no arquivo:\n------------------')
print(dados)