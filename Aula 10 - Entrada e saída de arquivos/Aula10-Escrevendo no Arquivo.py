'''
Escrevendo informações no arquivo

Diego Mendes Rodrigues
'''

# Função que mostra um registro formatado
# ---------------------------------------------------
def mostra_registro(linha:str):
    dados = linha.split(';')
    print('Nome...:', dados[0])
    print('Idade..:', dados[1])
    print('Cidade.:', dados[2])
    print('')

# Criando o arquivo do Banco de Dados (cadastro.txt)
# ---------------------------------------------------
arquivo = 'C:\\Users\Diego\\Documents\\Curso Python Básico - Scripts\\Aula 10 - Entrada e saída de arquivos\\cadastro.txt'
banco_de_dados = open(arquivo,'w')

pessoas = [
            ['Diego','34','Sao Paulo'],
            ['Regina','54','Paulinia'],
            ['Natalia','31','Rio de Janeiro']
         ]

# Escrevendo os registros no arquivo
for linha in pessoas:
    banco_de_dados.write(linha[0] + ';' + linha[1] + ';' + linha[2] + '\n')

# Fechando o Banco de Dados
banco_de_dados.close()


# Abrindo o Banco de Dados em Leitura e Escrita
# ---------------------------------------------------
banco_de_dados = open(arquivo,'r+')

for linha in banco_de_dados:
    mostra_registro(linha)

# Fechando o Banco de Dados
banco_de_dados.close()