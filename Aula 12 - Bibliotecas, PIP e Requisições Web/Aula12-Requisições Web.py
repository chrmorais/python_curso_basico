'''
Fazendo Requisições Web

Diego Mendes Rodrigues
'''

'''
Podemos importar bibliotecas padrões do Python
import sys
import time

Mas existem bibliotecas externas, milhares

Existe um gerenciador de pacotes no Python, o PIP
No CMD (ou Terminal dentro do PyCharm)
digitar pip (ou pip3)

Para instalar pacotes no Python e fazermos coisas que não podemos fazer com o Python Default
pip3 install <pacote>

Iremos utilizar a biblioteca 'requests' para realizar nossas Requisições Web
A biblioteca padrão do Python é 'urllib'
A 'requests' usa a 'urllib'
pip3 install requests

C:\\Users\\Diego>pip3 install requests
Collecting requests
  Downloading requests-2.12.1-py2.py3-none-any.whl (574kB)
    100% |################################| 583kB 533kB/s
Installing collected packages: requests
Successfully installed requests-2.12.1

Outra forma de usar o PIP
CMD (ou Terminal dentro do PyCharm)
python -m pip install <pacote>
'''

# Importando a Biblioteca 'requests'
import requests

'''
Quando navegamos na internet, abrimos o navegador
Fazemos uma requisição ao entrar em um site
O servidor responde com uma response
Continuamos a comunicação...

2 requisições (requests) principais
GET  - pegar uma página, um arquivo)
POST - enviar informações, formulários

Dentro dessas requisições (requests) existem os cabeçalhos

Status:
200 - OK
403 - Você não tem permissão para acessar
404 - Página não existe
500 - Erro interno do servidor
'''

# Fazendo um GET
requisicao = None
try:
    requisicao = requests.get('http://g1.globo.com')
except Exception as erro:
    print('Requisição deu erro:', erro)
    quit()

print(requisicao)
print(type(requisicao))
print('')

print('Status Code.:', requisicao.status_code)
print('Cookies.....:', requisicao.cookies)
print('Headers.....:', requisicao.headers)
print('Encoding....:', requisicao.encoding)
print('History.....:', requisicao.history)
print('URL.........:', requisicao.url)
print('Text........:\n', requisicao.text)

# Para fazer o parser de páginas, utilizar a biblioteca
# Beautiful Soup 4 (ou BS4)
# pip install bs4

