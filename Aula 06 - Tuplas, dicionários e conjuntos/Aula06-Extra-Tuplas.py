'''
Tuplas são estruturas de dados ordenadas e que não permitem alteração após sua criação.
Elas são usadas em muitas situações no Python.

Algo importante a se observar é que diferentemente das listas, as tuplas não podem ser alteradas, não havendo então
métodos de remoção, inserção ou alteração de elementos. Isto faz com que este seja um tipo de dados imprescindível
para a passagem de parâmetros para funções.

Uma das principais formas de utilizar as tuplas (e muitas vezes fazemos sem saber que estamos fazendo) é
o empacotamento de dados.

Fonte: http://www.programeempython.com.br/blog/estruturas-de-dados-em-python-tuplas/

Diego Mendes Rodrigues
'''

t = ()          # uma tupla vazia
print(t)

t1 = 1, 2, 3, 4 # uma tupla com valores
print('\n')
print(t1)

t2 = (1, 2, 3, 4)  # igual a t1, mas criada usando parênteses
print('\n')
print(t2)

t3 = ('abracadabra',)  # tuplas de 1 elemento são
# criadas com parênteses e uma virgula
print('\n')
print(t3)

t4 = 'yabadabadoo',  # ou só com o elemento e a vírgula
print('\n')
print(t4)

t5 = ('casa', 3, 4.12, t2, t1)  # tupla contendo objetos diferentes
print('\n')
print(t5)

# Podemos empacotar as coordenadas em uma tupla
x = 19
y = 22
ponto = x, y
print('\n')
print(ponto)

# Podemos desempacotar as coordenadas de uma tupla
del x
del y
x,y = ponto
print('\n')
print(x)
print(y)

# Um detalhe que deve ser comentado aqui é que empacotar dados sepre gera uma tupla, mas o inverso pode ser feito
# com qualquer sequência.