'''
Realizar uma busca no OMDb API - The Open Movies Database
Utilizar 'By Search'

Essa busca retorna várias páginas com resultados, exibir essas páginas

OMDb API - The Open Movies Database
www.omdbapi.com
The OMDb API is a free web service to obtain movie information, all content and images on the site are
contributed and maintained by our users.

Diego Mendes Rodrigues
'''

import requests
import json

def pesquisar_filme(pesquisar,pagina):
    try:
        site = 'http://www.omdbapi.com/?s=' + pesquisar + '&type=movie&page=' + str(pagina)
        req = requests.get(site)
        dados = req.text
        dicionario = json.loads(dados)
        return dicionario

    except Exception as erro:
        print('Erro na requisição:', erro)
        return None


# Função que exibe os detalhes do Filme
def exibir_detalhes(dicionario):
    print('Titulo......:', dicionario['Title'])
    print('Ano.........:', dicionario['Year'])
    print('IMDb ID.....:', dicionario['imdbID'])
    print('Poster......:', dicionario['Poster'])
    print('')

while True:
    filme = input('Qual filme iremos pesquisar (SAIR para encerrar):')

    if filme == '' or filme == 'SAIR' or filme == 'sair':
        print('Saindo.... Bye, bye!')
        exit()
    else:
        dicionario = pesquisar_filme(filme,1)

        if dicionario['Response'] == 'True':
            total_de_resultados = dicionario['totalResults']

            paginas = int(total_de_resultados)//10
            if int(total_de_resultados)%10 > 0:
                paginas += 1

            print('\nTotal de filmes encontrados....:', total_de_resultados,'\n')
            print('Páginas de retorno com filmes..:',paginas,'\n')

            lista_de_filmes = dicionario['Search']

            go = input('Pressione <ENTER> para ver a primeira página, ou S para sair: ')
            pagina_atual=1

            while go == '':
                # Exibindo até 10 filmes (1 página)
                for linha in lista_de_filmes:
                    exibir_detalhes(linha)
                    print('')

                # Solicitando <ENTER> para mostrar a próxima página
                proxima_pagina = pagina_atual + 1
                if proxima_pagina > paginas:
                    go = 'S'
                    input('Pressione <ENTER> para sair desta lista: ')
                else:
                    go = input('Pressione <ENTER> para ver a página ' + str(proxima_pagina) + ', ou S para sair: ')
                    if len(go) == 0:
                        pagina_atual += 1
                        dicionario = pesquisar_filme(filme,pagina_atual)
                        lista_de_filmes = dicionario['Search']

                print('')