'''
API - Application Programming Interface
JSON
Consultando listas de filmes

Podemos utilizar APIs através de requisições web

OMDb API - The Open Movies Database
www.omdbapi.com
The OMDb API is a free web service to obtain movie information, all content and images on the site are
contributed and maintained by our users.

JSON retorna praticamente um Dicionário do Python
'''

# Importando bibliotecas
import requests     # Requisições GET, POST, ...
import json         # Trabalhar com JSON

# Função que realiza a requisição no site OMDb API - The Open Movies Database e retorna um Dicionário
def requisicao(titulo):
    # Variaveis utilizadas
    req = None
    dados = None
    dicionario = None

    try:
        req = requests.get('http://www.omdbapi.com/?t='+titulo+'&type=movie')

        # O retorno req.text é um JSON
        dados = req.text

        # json.loads() pega um texto JSON e transdorma em um dicionário
        dicionario = json.loads(dados)
        return dicionario

    except Exception as erro:
        print('Erro na conexão:', erro)
        return None

# Função que exibe os detalhes do Filme
def exibir_detalhes(dicionario):
    print('Titulo......:', dicionario['Title'])
    print('Ano.........:', dicionario['Year'])
    print('Lançamento..:', dicionario['Released'])
    print('Duração.....:', dicionario['Runtime'])
    print('Genero......:', dicionario['Genre'])
    print('Atores......:', dicionario['Actors'])
    print('Diretor.....:', dicionario['Director'])
    print('Nota (IMDb).:', dicionario['imdbRating'])
    print('Resumo......:', dicionario['Plot'])
    print('Premios.....:', dicionario['Awards'])
    print('Poster......:', dicionario['Poster'])
    print('')

# Laço que solicita o filme ao usuário, depois exibe as informações
while True:
    filme = input('Qual titulo do filme iremos pesquisar (SAIR para fechar): ')

    if filme == '' or filme == 'SAIR' or filme == 'sair':
        print('Saindo! Bye!')
        print('')
        break
    else:
        dicionario = requisicao(filme)
        if dicionario['Response'] == 'False':
            print('Filme não encontrado')
            print('')
        else:
            exibir_detalhes(dicionario)