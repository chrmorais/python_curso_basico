'''
Utilizando a RegEx r'[\w\.-]+@[\w-]+\.[\w\.-]+' para pegar e-mails de sites

Diego Mendes Rodrigues
'''

import re
import requests

# Função que baixa o HTML do site
# --------------------------------------------------
def baixar_html(site_alvo):
    try:
        requisicao = requests.get(site_alvo)
        conteudo = requisicao.text
        requisicao.close()
        print('HTML adquirido de', site_alvo)
        return conteudo
    except Exception as erro:
        print('Falhar ao conectar no', site_alvo, ':', erro)
        return False


# Função que realiza a busca das ocorrencias de e-mail no conteúdo HTML
# --------------------------------------------------
def buscar_emails(conteudo_html):
    emails = re.findall(r'[\w\.-]+@[\w-]+\.[\w\.-]+', conteudo_html)
    if emails:
        print('E-mails:', emails)   # lista
        print('')
        return emails
    else:
        print('Nenhum e-mail encontrado')
        print('')
        return False


# Buscando e-mails (r'[\w\.-]+@[\w-]+\.[\w\.-]+')
# --------------------------------------------------
sites = [
    'http://lacoxinha.com.br/contato',
    'http://www.drsolutions.com.br/contato.php',
    'https://developer.mozilla.org/pt-BR/docs/User:domingues_1_santos@hotmail.com'
]

for site in sites:
    codido_html = baixar_html(site)
    if codido_html:
        buscar_emails(codido_html)
    print('')
