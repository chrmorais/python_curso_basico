'''
Fazer um programa que mostra em tempo real a cotação do dólar

Fazer um programa que mostra em tempo real a previsão do tempo

Diego Mendes Rodrigues
'''

import re
import requests
from datetime import *
import time

# Função que baixa o HTML do site
# --------------------------------------------------
def baixar_html(site_alvo):
    try:
        requisicao = requests.get(site_alvo)
        conteudo = requisicao.text
        requisicao.close()
        print('HTML adquirido de', site_alvo)
        return conteudo
    except Exception as erro:
        print('Falhar ao conectar no', site_alvo, ':', erro)
        return False


# Função que realiza a busca das cotações do dólar conteúdo HTML
# --------------------------------------------------
def buscar_cotacao(conteudo_html):
    cotacao = re.findall(r'[0-9],[0-9]+', conteudo_html)
    if cotacao:
        print('Cotação do dólar:', cotacao)   # lista
        if len(cotacao) > 1:
            print('Taxa de Compra.: R$', cotacao[0])
            print('Taxa de Venda..: R$', cotacao[1])
        else:
            print('Taxa de Venda..: R$', cotacao[0])
        print('')
        return cotacao
    else:
        print('Nenhuma cotação encontrada')
        print('')
        return False


# Função que realiza a busca da previsão do tempo em Paulínia
# --------------------------------------------------
def buscar_previsao(conteudo_html):
    previsao = re.findall(r'momento-temperatura..[0-9]*', conteudo_html)
    if previsao:
        print('Previsão em Paulínia:', previsao)  # lista
        previsao_quebrada = previsao[0].split('>')
        print('Hoje:', previsao_quebrada[1] + '°C')
        print('')
        return previsao_quebrada[1]
    else:
        print('Nenhuma previsão encontrada')
        print('')
        return False


# Laço infinito que realiza novas buscas de 10 em 10 segundos
# --------------------------------------------------

while True:
    # Delimitando Consultas
    print('##################################################')
    print('##################################################\n')

    # Buscando cotações do dólar
    # --------------------------------------------------

    print('--------------------------------------------------')
    print('COTAÇÃO DO DÓLAR')
    print('--------------------------------------------------')
    print('')

    sites = [
        'https://ptax.bcb.gov.br/ptax_internet/consultarUltimaCotacaoDolar.do',
        'http://g1.globo.com/economia/mercados/cotacoes/moedas/'
    ]

    for site in sites:
        codido_html = baixar_html(site)
        if codido_html:
            buscar_cotacao(codido_html)
        print('')


    # Buscando a Precisão do Tempo
    # --------------------------------------------------
    print('')
    print('--------------------------------------------------')
    print('PREVISÃO DO TEMPO')
    print('--------------------------------------------------')
    print('')

    sites = [
        'http://www.climatempo.com.br/previsao-do-tempo/cidade/504/paulinia-sp'
    ]

    for site in sites:
        codido_html = baixar_html(site)
        if codido_html:
            buscar_previsao(codido_html)
        print('')

    data_hora_agora = datetime.now()
    print('%d/%d/%d - %d:%d:%d' %(data_hora_agora.day, data_hora_agora.month, data_hora_agora.year,
                                  data_hora_agora.hour, data_hora_agora.minute, data_hora_agora.second))

    print('Nova consulta em 10s\n<Ctrl>+C ou <Ctrl>+<F12> para sair\n')
    time.sleep(10)
    print('')